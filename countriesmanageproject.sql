-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 09, 2021 at 08:31 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `countriesmanageproject`
--

-- --------------------------------------------------------

--
-- Table structure for table `continents`
--

CREATE TABLE `continents` (
  `id` int(11) UNSIGNED NOT NULL,
  `ctnt_name` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `continents`
--

INSERT INTO `continents` (`id`, `ctnt_name`) VALUES
(0, 'Chưa xác định'),
(1, 'Châu lục 1'),
(2, 'Châu lục 2'),
(3, 'Châu lục 3');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) UNSIGNED NOT NULL,
  `c_name` varchar(255) CHARACTER SET utf8mb4 DEFAULT '',
  `c_area` float(11,2) DEFAULT 0.00,
  `c_population` int(11) DEFAULT 0,
  `c_flag` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `c_joindate` date DEFAULT NULL,
  `c_continent_id` int(11) UNSIGNED DEFAULT 0,
  `c_topleveldomain` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `c_name`, `c_area`, `c_population`, `c_flag`, `c_joindate`, `c_continent_id`, `c_topleveldomain`, `created_at`, `updated_at`) VALUES
(1, 'Qualand', 101000.01, 30101000, '../Project/uploads/flag_01.png', '2011-01-01', 2, '.v1', '2021-06-24 17:19:01', '2021-06-25 16:30:27'),
(2, 'Zouland', 124010.02, 24022000, '../Project/uploads/flag_02.png', '2011-12-02', 1, '.v2', '2021-06-24 17:20:06', '2021-06-25 16:29:12'),
(3, 'Naurizilia', 300254.09, 33103000, '../Project/uploads/flag_03.png', '2013-11-03', 3, '.v3', '2021-06-24 17:20:51', '2021-06-25 16:29:45'),
(4, 'New Kinsgard', 14230.50, 24125000, '../Project/uploads/flag_04.png', '2011-05-04', 3, '.v4', '2021-06-24 17:21:32', '2021-06-25 09:20:05'),
(5, 'Argonelia', 25000.50, 25123000, '../Project/uploads/flag_05.png', '2013-12-05', 2, '.v5', '2021-06-24 17:22:52', '2021-06-25 09:24:55'),
(6, 'El Garola', 26123.60, 16521000, '../Project/uploads/flag_06.png', '2015-06-12', 2, '.v6', '2021-06-24 17:25:41', '2021-06-25 15:31:17'),
(7, 'Kirbastan', 19777.40, 17954000, '../Project/uploads/flag_07.png', '2020-10-07', 3, '.v7', '2021-06-24 17:26:49', '2021-06-25 09:38:20'),
(8, 'Urgania', 18012.80, 18123000, '../Project/uploads/flag_08.png', '2014-09-18', 2, '.v8', '2021-06-24 17:27:16', '2021-06-25 09:39:18'),
(9, 'Esugal d\'Sud', 25999.09, 29111000, '../Project/uploads/flag_09.png', '2015-09-13', 3, '.v9', '2021-06-24 17:27:58', '2021-06-25 15:35:26'),
(10, 'Esugal d\'Nord', 31010.10, 23101000, '../Project/uploads/flag_10.png', '2015-10-10', 3, '.w0', '2021-06-24 17:29:21', '2021-07-09 01:12:33'),
(11, 'Irlevint Islands', 11111.11, 14111000, '../Project/uploads/flag_11.png', '2011-02-11', 1, '.w1', '2021-06-24 17:30:01', '2021-06-25 16:27:47'),
(12, 'Orvelious', 125012.00, 40212000, '../Project/uploads/flag_12.png', '2012-06-08', 1, '.w2', '2021-06-25 11:16:46', '2021-06-25 15:35:08'),
(13, 'Centrego', 130313.00, 39013000, '../Project/uploads/flag_13.png', '2013-03-13', 1, '.w3', '2021-06-25 11:22:18', '2021-06-25 15:35:15'),
(14, 'Markolenia', 79014.00, 14894000, '../Project/uploads/flag_14.png', '2014-06-14', 3, '.w4', '2021-06-25 15:44:16', '2021-06-25 15:44:16'),
(15, 'Polahertos', 215640.16, 15915000, '../Project/uploads/flag_15.png', '2015-01-15', 2, '.w5', '2021-06-25 16:04:34', '2021-06-25 16:04:34'),
(16, 'Berkopan', 30160.16, 16555000, '../Project/uploads/flag_16.png', '2012-06-16', 1, '.w6', '2021-07-09 01:07:12', '2021-07-09 01:07:12'),
(17, 'Iheragdah', 32171.17, 17555000, '../Project/uploads/flag_17.png', '2012-07-17', 1, '.w7', '2021-07-09 01:08:03', '2021-07-09 01:08:03'),
(18, 'East Quindalays', 50000.18, 18111000, '../Project/uploads/flag_18.png', '2015-05-05', 2, '.w8', '2021-07-09 01:09:16', '2021-07-09 01:09:16'),
(19, 'West Quindalays', 45999.19, 19111000, '../Project/uploads/flag_19.png', '2015-05-05', 2, '.w9', '2021-07-09 01:11:11', '2021-07-09 01:11:11'),
(20, 'Great Fiderway', 202020.00, 2919000, '../Project/uploads/flag_20.png', '2020-02-20', 3, '.v0', '2021-07-09 01:14:14', '2021-07-09 01:14:14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `continents`
--
ALTER TABLE `continents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `c_continent_id` (`c_continent_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `countries`
--
ALTER TABLE `countries`
  ADD CONSTRAINT `countries_fk_1` FOREIGN KEY (`c_continent_id`) REFERENCES `continents` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
