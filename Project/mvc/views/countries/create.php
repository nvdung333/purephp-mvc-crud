<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CREATE</title>
    
    <!-- Bootstrap CSS -->
    <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous"> -->

    <!-- Nhúng Bootstrap (tự host) -->
    <link rel="stylesheet" href="bootstrap-4.6.0/css/bootstrap.css">
    <script src="bootstrap-4.6.0/js/bootstrap.bundle.js"></script>

    <style>
    .flex-container {display: flex;}
    .flex-container > div {margin-right: 30px;}
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h3>Nhập thông tin thêm mới</h3>
            <form name="createcountry" method="post" action="<?php SITE_URL ?>index.php?controller=countries&action=store" enctype="multipart/form-data">

                <div class="form-group">
                    <label class="form-label">Tên quốc gia / vùng lãnh thổ:</label>
                    <span style="color:red">*</span>
                    <input name="c_name" type="text" class="form-control" value="">
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="form-label">Diện tích (km<sup>2</sup>):</label>
                            <input name="c_area" type="text" class="form-control" value="">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="form-label">Dân số (người):</label>
                            <input name="c_population" type="text" class="form-control" value="">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="form-label">Top-level Domain:</label>
                            <input name="c_topleveldomain" type="text" class="form-control" value="">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="form-label">Châu lục:</label>
                    <div class="flex-container">
                        <div class="form-check">
                            <input name="c_continent_id" type="radio" class="form-check-input" value="0" >Chưa xác định
                        </div>
                        <div class="form-check">
                            <input name="c_continent_id" type="radio" class="form-check-input" value="1" >Châu lục 1
                        </div>
                        <div class="form-check">
                            <input name="c_continent_id" type="radio" class="form-check-input" value="2" >Châu lục 2
                        </div>
                        <div class="form-check">
                            <input name="c_continent_id" type="radio" class="form-check-input" value="3" >Châu lục 3
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="form-label">Ngày gia nhập tổ chức:</label>
                            <input name="c_joindate" type="date" class="form-control" value="">
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="form-label">Quốc kỳ:</label><br>
                    <input name="c_flag" type="file" class="form-control">
                </div>

                <button type="submit" class="btn btn-primary" onclick='return confirm("Bạn có chắc chắn muốn thêm?")'>Thêm mới (nút 1)</button>

                <input type="submit" onclick="return xacnhan()" class="btn btn-primary" value="Thêm mới (nút 2)">
                <script>
                    function xacnhan() { return confirm("Bạn đã chắc chắn xác nhận muốn thêm hay chưa?"); }
                </script>

                <a class="btn btn-info" href="<?php echo SITE_URL?>index.php?controller=countries&action=read" onclick='return confirm("Bạn có chắc chắn muốn trở về?")' role="button">Trở về</a>
            </form>
        </div>
    </div>    
</div>
</body>
</html>