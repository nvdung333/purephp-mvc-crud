<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>READ</title>

    <!-- Bootstrap CSS -->
    <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous"> -->

    <!-- Nhúng Bootstrap (tự host) -->
    <link rel="stylesheet" href="bootstrap-4.6.0/css/bootstrap.css">
    <script src="bootstrap-4.6.0/js/bootstrap.bundle.js"></script>

</head>
<?php
$request_keyword = isset($_REQUEST["keyword"]) ? $_REQUEST["keyword"] : "";
$request_orderby = isset($_REQUEST["orderby"]) ? $_REQUEST["orderby"] : "";
$request_orderdir = isset($_REQUEST["orderdir"]) ? $_REQUEST["orderdir"] : "";
$request_continent = isset($_REQUEST["continent"]) ? $_REQUEST["continent"] : "";
$request_limitrecord = isset($_REQUEST["limitrecord"]) ? $_REQUEST["limitrecord"] : (int)$limitRecord;
?>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">

                <?php
                if (isset($_SESSION["flash_message"]))
                {?>
                    <div class="alert alert-success" role="alert">
                        <?php
                        echo $_SESSION["flash_message"];
                        unset($_SESSION["flash_message"]);
                        ?>
                    </div>
                <?php
                }
                
                if (isset($_SESSION["error"]))
                {?>
                    <div class="alert alert-danger" role="alert">
                        <?php
                        echo $_SESSION["error"];
                        unset($_SESSION["error"]);
                        ?>
                    </div>
                <?php
                }?>

                <h1 style="text-align: center">Danh sách các quốc gia và vùng lãnh thổ</h1>

                <div style="margin: 20px 0px">
                    <a class="btn btn-primary" href="<?php echo SITE_URL?>index.php?controller=countries&action=create" role="button">Thêm mới</a>

                    <!-- <button class="btn btn-success" onclick="return F5()">Refresh</button> -->
                    <a class="btn btn-success" href="<?php echo SITE_URL?>index.php?controller=countries&action=read" role="button">Refresh</a>
                    <!-- <script>
                        function F5(){window.location.reload()}
                    </script> -->
                </div>
                

                <!-- Phần lọc và tìm kiếm -->
                <form name="search" action="" method="get">   
                    <div class="row">
                        <div class="col-md-4">
                            <label class="form-label">Tìm kiếm:</label>
                            <input name="keyword" type="text" class="form-control" value="<?php echo $request_keyword?>" placeholder="Nhập từ khóa tìm kiếm...">
                        </div>

                        <div class="col-md-2">
                            <label class="form-label">Sắp xếp theo:</label>
                            <select name="orderby" class="custom-select">
                                <option value="">Chọn theo...</option>
                                <option value="c_id" <?php echo ($request_orderby=="c_id") ? "selected" : "" ?>>ID</option>
                                <option value="c_name" <?php echo ($request_orderby=="c_name") ? "selected" : "" ?>>Name</option>
                                <option value="c_area" <?php echo ($request_orderby=="c_area") ? "selected" : "" ?>>Area</option>
                                <option value="c_population" <?php echo ($request_orderby=="c_population") ? "selected" : "" ?>>Population</option>
                                <option value="ctnt_name" <?php echo ($request_orderby=="ctnt_name") ? "selected" : "" ?>>Continent</option>
                                <option value="c_joindate" <?php echo ($request_orderby=="c_joindate") ? "selected" : "" ?>>Join Date</option>
                            </select>
                        </div>

                        <div class="col-md-2">
                            <label class="form-label">Hướng sắp xếp:</label>
                            <select name="orderdir" class="custom-select">
                                <option value="">Chọn hướng...</option>
                                <option value="ASC" <?php echo ($request_orderdir=="ASC") ? "selected" : "" ?>>Tăng dần</option>
                                <option value="DESC" <?php echo ($request_orderdir=="DESC") ? "selected" : "" ?>>Giảm dần</option>
                            </select>
                        </div>

                        <div class="col-md-2">
                            <label class="form-label">Lọc theo continent:</label>
                            <select name="continent" class="custom-select">
                                <option value="">Chọn continent...</option>
                                <option value="ctnt_0" <?php echo ($request_continent=="ctnt_0") ? "selected" : "" ?>>Chưa xác định</option>
                                <option value="ctnt_1" <?php echo ($request_continent=="ctnt_1") ? "selected" : "" ?>>Châu lục 1</option>
                                <option value="ctnt_2" <?php echo ($request_continent=="ctnt_2") ? "selected" : "" ?>>Châu lục 2</option>
                                <option value="ctnt_3" <?php echo ($request_continent=="ctnt_3") ? "selected" : "" ?>>Châu lục 3</option>
                            </select>
                        </div>

                        <input type="hidden" name="limitrecord" value="<?php echo $request_limitrecord ?>" />

                        <div class="col-md-2">
                            <label class="form-label">Kết quả: </label>
                            <span style="font-weight:bold"> <?php echo $count->total?> KẾT QUẢ</span>
                            <div> <button class="btn btn-info">Lọc kết quả</button> </div>
                        </div>
                    </div>
                </form>
               
                <!-- Phân trang (head) -->
                <nav aria-label="Page navigation example">
                    <ul class="pagination">

                        <?php
                            $page = isset($_GET["page"]) ? (int)$_GET["page"] : 1;
                            $previousPage = ($page > 1) ? $page-1 : 0;
                            $nextPage = $page+1;
                        ?>

                        <!-- Hiển thị button Previous -->
                        <?php
                            if ($previousPage > 0)
                            {
                                $tmp = $_GET;
                                $tmp["page"] = $previousPage;
                                $linkPrevious = "index.php?".http_build_query($tmp);
                                ?>
                                <li class="page-item"><a class="page-link" href="<?php echo $linkPrevious ?>">Previous</a></li>
                                <?php
                            }
                            else
                            { ?>
                                <li class="page-item disabled"><a class="page-link" href="#">Previous</a></li>
                                <?php
                            } ?>

                        <!-- Hiển thị các button Page -->
                        <?php
                            for($btnPage=1; $btnPage<=$pageTotal; $btnPage++)
                            { 
                                if ($page == $btnPage)
                                { $pageClassAction = "active"; }
                                else
                                { $pageClassAction = ""; }

                                $tmp = $_GET;
                                $tmp["page"] = $btnPage;
                                $linkPage = "index.php?".http_build_query($tmp);
                                ?>
                                <li class="page-item <?php echo $pageClassAction ?>"><a class="page-link" href="<?php echo $linkPage ?>"><?php echo $btnPage ?></a></li>
                                <?php
                            } ?>

                        <!-- Hiển thị button Next -->
                        <?php
                            if ($nextPage < $btnPage)
                            {
                                $tmp = $_GET;
                                $tmp["page"] = $nextPage;
                                $linkNext = "index.php?".http_build_query($tmp);
                                ?>
                                <li class="page-item"><a class="page-link" href="<?php echo $linkNext ?>">Next</a></li>
                                <?php
                            }
                            else
                            { ?>
                                <li class="page-item disabled"><a class="page-link" href="#">Next</a></li>
                                <?php
                            } ?>

                    </ul>
                </nav>

                <!-- Phần danh sách -->
                <table class="table table-striped table-borderless">
                    <thead class="table-dark">
                        <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Name</th>
                        <th scope="col">Area</th>
                        <th scope="col">Population</th>
                        <th scope="col">Continent</th>
                        <th scope="col">Domain</th>
                        <th scope="col">Join Date</th>
                        <th scope="col">Created_at</th>
                        <th scope="col">Updated_at</th>
                        <th scope="col">Options</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (is_array($countries) && !empty($countries))
                        {
                            foreach($countries as $keyCountries=>$countries) 
                            {?>
                                <tr>
                                <td><?php echo $countries->c_id ?></td>
                                <td>
                                    <img src="<?php echo $countries->c_flag ?>" alt="flag_img" style="width: 30px; height: 20px;">
                                    <?php echo $countries->c_name ?>
                                </td>
                                <td><?php echo $countries->c_area ?></td>
                                <td><?php echo $countries->c_population ?></td>
                                <td><?php echo $countries->ctnt_name ?></td>
                                <td><?php echo $countries->c_topleveldomain ?></td>
                                <td><?php echo $countries->c_joindate ?></td>
                                <td><?php echo $countries->created_at ?></td>
                                <td><?php echo $countries->updated_at ?></td>
                                <td> 
                                    <a class="btn btn-warning" href="<?php echo SITE_URL?>index.php?controller=countries&action=edit&id=<?php echo $countries->c_id?>" role="button">Sửa</a>
                                    <a class="btn btn-danger" href="<?php echo SITE_URL?>index.php?controller=countries&action=delete&id=<?php echo $countries->c_id?>" role="button">Xóa</a>
                                </td>
                                </tr>
                            <?php
                            }
                        }
                        ?>  
                    </tbody>
                    <tfoot class="table-secondary">
                        <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Name</th>
                        <th scope="col">Area</th>
                        <th scope="col">Population</th>
                        <th scope="col">Continent</th>
                        <th scope="col">Domain</th>
                        <th scope="col">Join Date</th>
                        <th scope="col">Created_at</th>
                        <th scope="col">Updated_at</th>
                        <th scope="col">Options</th>
                        </tr>
                    </tfoot>
                </table>

                <!-- Phân trang (foot) -->
                <nav aria-label="Page navigation example">
                    <ul class="pagination">

                        <?php
                            $page = isset($_GET["page"]) ? (int)$_GET["page"] : 1;
                            $previousPage = ($page > 1) ? $page-1 : 0;
                            $nextPage = $page+1;
                        ?>

                        <!-- Hiển thị button Previous -->
                        <?php
                            if ($previousPage > 0)
                            {
                                $tmp = $_GET;
                                $tmp["page"] = $previousPage;
                                $linkPrevious = "index.php?".http_build_query($tmp);
                                ?>
                                <li class="page-item"><a class="page-link" href="<?php echo $linkPrevious ?>">Previous</a></li>
                                <?php
                            }
                            else
                            { ?>
                                <li class="page-item disabled"><a class="page-link" href="#">Previous</a></li>
                                <?php
                            } ?>

                        <!-- Hiển thị các button Page -->
                        <?php
                            for($btnPage=1; $btnPage<=$pageTotal; $btnPage++)
                            { 
                                if ($page == $btnPage)
                                { $pageClassAction = "active"; }
                                else
                                { $pageClassAction = ""; }

                                $tmp = $_GET;
                                $tmp["page"] = $btnPage;
                                $linkPage = "index.php?".http_build_query($tmp);
                                ?>
                                <li class="page-item <?php echo $pageClassAction ?>"><a class="page-link" href="<?php echo $linkPage ?>"><?php echo $btnPage ?></a></li>
                                <?php
                            } ?>

                        <!-- Hiển thị button Next -->
                        <?php
                            if ($nextPage < $btnPage)
                            {
                                $tmp = $_GET;
                                $tmp["page"] = $nextPage;
                                $linkNext = "index.php?".http_build_query($tmp);
                                ?>
                                <li class="page-item"><a class="page-link" href="<?php echo $linkNext ?>">Next</a></li>
                                <?php
                            }
                            else
                            { ?>
                                <li class="page-item disabled"><a class="page-link" href="#">Next</a></li>
                                <?php
                            } ?>

                    </ul>
                </nav>
                <?php
                    // echo "<pre>Button Page Number: ".$btnPage;
                    // echo "<br>Previous Page: ".$previousPage;
                    // echo "<br>Current Page: ".$page;
                    // echo "<br>Next Page: ".$nextPage."</pre>";
                ?>

                <!-- Chọn số bản ghi trên 1 trang -->
                <div class="row">
                    <div class="col-md-12">
                        <form class="" name="" action="" method="get">       
                            <div class="form-group row">

                                <?php
                                if (isset($_REQUEST["keyword"]) or isset($_REQUEST["orderby"]) or isset($_REQUEST["orderdir"]) or isset($_REQUEST["continent"]))
                                { ?>
                                    <input type="hidden" name="keyword" value="<?php echo $request_keyword ?>" />
                                    <input type="hidden" name="orderby" value="<?php echo $request_orderby ?>" />
                                    <input type="hidden" name="orderdir" value="<?php echo $request_orderdir ?>" />
                                    <input type="hidden" name="continent" value="<?php echo $request_continent ?>" />
                                    <?php
                                } ?>
                                
                                <label for="submitLimitRecord" class="col-form-label col-md-1">Số bản ghi: </label>          
                                
                                <div class="col-md-1">
                                    <input name="limitrecord" id="submitLimitRecord" type="text" class="form-control" value="<?php echo $request_limitrecord ?>"> 
                                </div>
                                
                                <div class="col-md-2">
                                    <button class="btn btn-outline-info">Xác nhận</button>
                                </div>
                                
                            </div>
                        </form>
                        <?php 
                            // echo "<pre>limitRecord: ".$limitRecord."</pre>";
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>