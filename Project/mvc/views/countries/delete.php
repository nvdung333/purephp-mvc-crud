<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DELETE</title>

    <!-- Bootstrap CSS -->
    <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous"> -->

    <!-- Nhúng Bootstrap (tự host) -->
    <link rel="stylesheet" href="bootstrap-4.6.0/css/bootstrap.css">
    <script src="bootstrap-4.6.0/js/bootstrap.bundle.js"></script>
    
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">

            <?php
            if (isset($_SESSION["delete_message"]))
            {?>
                <div class="alert alert-danger" role="alert">
                    <?php
                    echo $_SESSION["delete_message"];
                    unset($_SESSION["delete_message"]);
                    ?>
                </div>
            <?php
            }?>

            <form name="deletecountry" method="post" action="<?php SITE_URL ?>index.php?controller=countries&action=destroy" enctype="multipart/form-data">  
                <input type="hidden" name="onlyid" value="<?php echo $country->c_id ?>" />

                <div class="form-group">
                    <label class="form-label">Quốc gia / Vùng lãnh thổ:</label>
                    <input name="c_name" type="text" class="form-control" value="<?php echo $country->c_name ?>" readonly>
                </div>

                <div class="form-group">
                    <label>Quốc kì:</label>
                    <?php if (isset($country->c_flag) && strlen($country->c_flag) > 0) { ?>
                        <br><img src="<?php echo $country->c_flag ?>" alt="" style="width: 300px; height: 200px;">
                        <?php
                    } ?>
                </div>

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="form-label">Dân số (người):</label>
                            <input name="c_population" type="text" class="form-control" value="<?php echo $country->c_population ?>" readonly>
                        </div>
                    </div>
                    
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="form-label">Diện tích (km<sup>2</sup>):</label>
                            <input name="c_area" type="text" class="form-control" value="<?php echo $country->c_area ?>" readonly>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="form-label">Châu lục:</label>
                            <input name="ctnt_name" type="text" class="form-control" value="<?php echo $country->ctnt_name ?>" readonly>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="form-label">Top-level Domain:</label>
                            <input name="ctnt_name" type="text" class="form-control" value="<?php echo $country->c_topleveldomain ?>" readonly>
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-danger" onclick="return confirm('Bạn có chắc chắn muốn xóa?')">Xoá</button>            

                <a class="btn btn-info" href="<?php echo SITE_URL?>index.php?controller=countries&action=read" onclick='return confirm("Bạn có chắc chắn muốn trở về?")' role="button">Trở về</a>

            </form>
        </div>
    </div>
</div>
</body>
</html>