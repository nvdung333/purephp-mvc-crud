<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>UPDATE</title>

    <!-- Bootstrap CSS -->
    <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous"> -->
    
    <!-- Nhúng Bootstrap (tự host) -->
    <link rel="stylesheet" href="bootstrap-4.6.0/css/bootstrap.css">
    <script src="bootstrap-4.6.0/js/bootstrap.bundle.js"></script>

    <style>
    .flex-container {display: flex;}
    .flex-container > div {margin-right: 30px;}
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">

            <?php
            if (isset($_SESSION["update_message"]))
            {?>
                <div class="alert alert-warning" role="alert">
                    <?php
                    echo $_SESSION["update_message"];
                    unset($_SESSION["update_message"]);
                    ?>
                </div>
            <?php
            }?>

            <form name="updatecountry" method="post" action="<?php SITE_URL ?>index.php?controller=countries&action=update" enctype="multipart/form-data">
            <input type="hidden" name="onlyid" value="<?php echo $country->c_id ?>" />

                <div class="form-group">
                    <label class="form-label">Tên quốc gia / vùng lãnh thổ:</label>
                    <span style="color:red">*</span>
                    <input name="c_name" type="text" class="form-control" value="<?php echo $country->c_name ?>">
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="form-label">Diện tích (km<sup>2</sup>):</label>
                            <input name="c_area" type="text" class="form-control" value="<?php echo $country->c_area ?>">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="form-label">Dân số (người):</label>
                            <input name="c_population" type="text" class="form-control" value="<?php echo $country->c_population ?>">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="form-label">Top-level Domain:</label>
                            <input name="c_topleveldomain" type="text" class="form-control" value="<?php echo $country->c_topleveldomain ?>">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="form-label">Châu lục:</label>
                    <div class="flex-container">
                        <div class="form-check">
                            <?php $checked0 = ($country->c_continent_id == 0) ? "checked" : ""; ?>
                            <input name="c_continent_id" type="radio" class="form-check-input" value="0" <?php echo $checked0 ?>>Chưa xác định
                        </div>
                        <div class="form-check">
                            <?php $checked1 = ($country->c_continent_id == 1) ? "checked" : ""; ?>
                            <input name="c_continent_id" type="radio" class="form-check-input" value="1" <?php echo $checked1 ?>>Châu lục 1
                        </div>
                        <div class="form-check">
                            <?php $checked2 = ($country->c_continent_id == 2) ? "checked" : ""; ?>
                            <input name="c_continent_id" type="radio" class="form-check-input" value="2" <?php echo $checked2 ?>>Châu lục 2
                        </div>
                        <div class="form-check">
                            <?php $checked3 = ($country->c_continent_id == 3) ? "checked" : ""; ?>
                            <input name="c_continent_id" type="radio" class="form-check-input" value="3" <?php echo $checked3 ?>>Châu lục 3
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="form-label">Ngày gia nhập tổ chức:</label>
                            <input name="c_joindate" type="date" class="form-control" value="<?php echo $country->c_joindate ?>">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="form-label">Quốc kỳ:</label><br>
                    <?php if (isset($country->c_flag) && strlen($country->c_flag) > 0) { ?>
                        <img src="<?php echo $country->c_flag ?>" alt="" style="width: 150px; height: 100px;">
                        <?php
                    } ?><br>
                    <input name="c_flag" type="file" class="form-control">
                </div>

                <button type="submit" class="btn btn-primary" onclick='return confirm("Bạn có chắc chắn muốn cập nhật?")'>Cập nhật (nút 1)</button>

                <input type="submit" onclick="return xacnhan()" class="btn btn-primary" value="Cập nhật (nút 2)">
                <script>
                    function xacnhan() { return confirm("Bạn có chắc chắn muốn sửa hay không?"); }
                </script>

                <a class="btn btn-info" href="<?php echo SITE_URL?>index.php?controller=countries&action=read" onclick='return confirm("Bạn có chắc chắn muốn trở về?")' role="button">Trở về</a>
            </form>
        </div>
    </div>    
</div>
</body>
</html>