<?php
class Database
{
    public $connection;
    
    const DATABASE_SERVER = "localhost";
    const DATABASE_USER = "root";
    const DATABASE_PASSWORD = "";
    const DATABASE_NAME = "countriesmanageproject";


    public function __construct()
    {
        if (!$this->connection)
        {
            try
            {
                $serverName = self::DATABASE_SERVER;
                $databaseName = self::DATABASE_NAME;
                $username = self::DATABASE_USER;
                $password = self::DATABASE_PASSWORD;
                $this->connection = new PDO("mysql:host=$serverName;dbname=$databaseName", $username, $password);

                //xử lý lỗi ngoại lệ khi kết nối PDO
                $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);  
            }
            catch (PDOException $e)
            {
                echo $e->getMessage();
                exit();
            }
        }
    }
}
