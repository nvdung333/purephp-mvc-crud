<?php
class Router
{
    public function run()
    {
        echo "<br>".__METHOD__;

        //Controller mặc định
        $defaultController = "countries";

        //Method (phương thức) mặc định
        $defaultAction = "read";

        
        $controller = isset($_REQUEST["controller"]) && ($_REQUEST["controller"]) ? ($_REQUEST["controller"]) : $defaultController;
        $action = isset($_REQUEST["action"]) && ($_REQUEST["action"]) ? ($_REQUEST["action"]) : $defaultAction;


        $controller = ucfirst($controller); //Xử lý biến, viết hoa chữ cái đầu
        $controller = $controller."Controller"; //Chọn Controller


        /*
        // Kiểu viết thủ công:
        $controllerObject = new CountriesController();
        $controllerObject->read(); */
        // Kiểu viết linh hoạt:
        $controllerObject = new $controller();
        $controllerObject->$action();
    }
}
