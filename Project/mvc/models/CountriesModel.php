<?php
class CountriesModel extends Database
{
    //bảng làm việc
    public $table1 = "countries";
    public $table2 = "continents";

    //lấy ra nhiều bản ghi
    public function getAll($dataRaw)
    {
        echo "<br>" . __METHOD__;
        // echo "<pre>Print dataRaw: ";
        // print_r($dataRaw);
        // echo "</pre>";

        $dataBind = [];
        $dataBind[] = $dataRaw["startRecord"];
        $dataBind[] = $dataRaw["limitRecord"];
        // echo "<pre>Print dataBind: ";
        // print_r($dataBind);
        // echo "</pre>";

        $sqlSelect = "  SELECT *,$this->table1.id as c_id FROM $this->table1
                        LEFT JOIN $this->table2 ON $this->table1.c_continent_id = $this->table2.id
                        ORDER BY c_id
                        LIMIT $dataBind[0],$dataBind[1]
                     ";
        $stmt = $this->connection->prepare($sqlSelect);
        $stmt->execute();
        $result = $stmt->setFetchMode(PDO::FETCH_OBJ);
        $countries = $stmt->fetchAll();

        // echo "<pre>";
        // var_dump($sqlSelect);
        // echo "</pre>";

        // echo "<pre>";
        // print_r($countries);
        // echo "</pre>";
        return $countries;  
    }

    //Đếm số tổng bản ghi
    public function count1()
    {
        echo "<br>" . __METHOD__;
        $sqlCount = "SELECT COUNT(*) AS total FROM $this->table1";
        $stmt = $this->connection->prepare($sqlCount);
        $stmt->execute();
        $result = $stmt->setFetchMode(PDO::FETCH_OBJ);
        $count = $stmt->fetchObject();

        // echo "<pre>";
        // print_r($count);
        // echo "</pre>";
        return $count;
    }

    //lấy ra 1 bản ghi duy nhất
    public function getSingle($id)
    {
        echo "<br>" . __METHOD__;
        // $sqlSelect = "SELECT * FROM $this->table1 WHERE id = ? LIMIT 1";
        $sqlSelect = "  SELECT *,$this->table1.id as c_id FROM $this->table1
                        LEFT JOIN $this->table2 ON $this->table1.c_continent_id = $this->table2.id
                        WHERE $this->table1.id = ?
                        LIMIT 1";
        $stmt = $this->connection->prepare($sqlSelect);
        $stmt->execute([$id]);
        $result = $stmt->setFetchMode(PDO::FETCH_OBJ);
        $country = $stmt->fetchObject();

        // echo "<pre>";
        // print_r($country);
        // echo "</pre>";
        return $country;  
    }

    //Lưu mới 1 bản ghi
    public function store(array $dataBind)
    {
        echo "<br>" . __METHOD__;
        echo "<pre>";
        print_r($dataBind);
        echo "</pre>";

        // $sqlInsert = "INSERT INTO $this->table1 (`c_name`, `c_area`, `c_population`, `c_flag`, `c_joindate`, `c_continent_id`, `created_at`, `updated_at`) VALUES ('$dataBind[0]','$dataBind[1]','$dataBind[2]','$dataBind[3]','$dataBind[4]','$dataBind[5]','$dataBind[6]','$dataBind[7]')";

        $sqlInsert = "INSERT INTO $this->table1 (`c_name`, `c_area`, `c_population`, `c_flag`, `c_joindate`, `c_continent_id`, `c_topleveldomain`, `created_at`, `updated_at`) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ? )";
        $stmtInsert = $this->connection->prepare($sqlInsert);
        $resultInsert = $stmtInsert->execute($dataBind);
        return $resultInsert;
    }

    //Check duplicate
    public function checkduplicate1(array $dataRaw)
    {
        echo "<br>" . __METHOD__;
        echo "<pre>";
        print_r($dataRaw);
        echo "</pre>";

        $dataBindSQL = [];
        $dataBindSQL[] = $dataRaw["c_name"];
        $dataBindSQL[] = $dataRaw["c_topleveldomain"];

        echo "<pre>";
        print_r($dataBindSQL);
        echo "</pre>";

        $sqlCheckDuplicate = "  SELECT COUNT(*) AS total FROM $this->table1
                                WHERE   `c_name` = \"$dataBindSQL[0]\"
                                OR      `c_topleveldomain` = \"$dataBindSQL[1]\"";

        $stmtCheckDuplicate = $this->connection->prepare($sqlCheckDuplicate);
        $stmtCheckDuplicate->execute($dataBindSQL);
        $resultCheckDuplicate = $stmtCheckDuplicate->setFetchMode(PDO::FETCH_OBJ);
        $duplicate = $stmtCheckDuplicate->fetchObject();
      
        echo "<pre>";
        var_dump($sqlCheckDuplicate);
        echo "</pre>";

        // echo "<pre>";
        // var_dump($duplicate);
        // echo "</pre>";

        echo "<pre>";
        print_r($duplicate);
        echo "</pre>";

        if ($duplicate->total > 0) { return false; }
        else { return true; }
    }

    //Sửa 1 bản ghi
    public function update(array $dataRaw)
    {
        echo "<br>" . __METHOD__;
        echo "<pre>";
        print_r($dataRaw);
        echo "</pre>";

        $dataBindSQL = [];

        // SQL theo hướng ? chống SQL Injection
        // Sử dụng .= để nối câu SQL
        $sqlUpdate = "UPDATE $this->table1 SET ";

        if (isset($dataRaw['c_name']))
        {
            $sqlUpdate .= "`c_name` = ?";
            $dataBindSQL[] = $dataRaw['c_name'];
        }
        if (isset($dataRaw['c_area']))
        {
            $sqlUpdate .= ", `c_area` = ?";
            $dataBindSQL[] = $dataRaw['c_area'];
        }
        if (isset($dataRaw['c_population']))
        {
            $sqlUpdate .= ", `c_population` = ?";
            $dataBindSQL[] = $dataRaw['c_population'];
        }
        if (isset($dataRaw['c_flag']))
        {
            $sqlUpdate .= ", `c_flag` = ?";
            $dataBindSQL[] = $dataRaw['c_flag'];
        }
        if (isset($dataRaw['c_joindate']))
        {
            $sqlUpdate .= ", `c_joindate` = ?";
            $dataBindSQL[] = $dataRaw['c_joindate'];
        }
        if (isset($dataRaw['c_continent_id']))
        {
            $sqlUpdate .= ", `c_continent_id` = ?";
            $dataBindSQL[] = $dataRaw['c_continent_id'];
        }
        if (isset($dataRaw['c_topleveldomain']))
        {
            $sqlUpdate .= ", `c_topleveldomain` = ?";
            $dataBindSQL[] = $dataRaw['c_topleveldomain'];
        }
        if (isset($dataRaw['updated_at']))
        {
            $sqlUpdate .= ", `updated_at` = ?";
            $dataBindSQL[] = $dataRaw['updated_at'];
        }
        if (isset($dataRaw['id']))
        {
            $sqlUpdate .= " WHERE `id` = ?";
            $dataBindSQL[] = $dataRaw['id'];
        }

        echo "<pre>";
        var_dump($sqlUpdate);
        echo "</pre>";

        echo "<pre>";
        print_r($dataBindSQL);
        echo "</pre>";
        
        $stmtUpdate = $this->connection->prepare($sqlUpdate);
        $resultUpdate = $stmtUpdate->execute($dataBindSQL);
        return $resultUpdate;
    }

    //Check duplicate
    public function checkduplicate2(array $dataRaw)
    {
        echo "<br>" . __METHOD__;
        echo "<pre>";
        print_r($dataRaw);
        echo "</pre>";

        $dataBindSQL = [];
        $dataBindSQL[] = $dataRaw["c_name"];
        $dataBindSQL[] = $dataRaw["c_topleveldomain"];
        $dataBindSQL[] = $dataRaw["id"];

        echo "<pre>";
        print_r($dataBindSQL);
        echo "</pre>";
        
        $sqlCheckDuplicate = "  SELECT COUNT(*) AS total FROM $this->table1
                                WHERE   ( `c_name` = \"$dataBindSQL[0]\"
                                OR      `c_topleveldomain` = \"$dataBindSQL[1]\" )
                                AND     `id` != '$dataBindSQL[2]'";

        $stmtCheckDuplicate = $this->connection->prepare($sqlCheckDuplicate);
        $stmtCheckDuplicate->execute($dataBindSQL);
        $resultCheckDuplicate = $stmtCheckDuplicate->setFetchMode(PDO::FETCH_OBJ);
        $duplicate = $stmtCheckDuplicate->fetchObject();
        
        echo "<pre>";
        var_dump($sqlCheckDuplicate);
        echo "</pre>";

        // echo "<pre>";
        // var_dump($duplicate);
        // echo "</pre>";

        echo "<pre>";
        print_r($duplicate);
        echo "</pre>";

        if ($duplicate->total > 0) { return false; }
        else { return true; }
    }

    //Xóa 1 bản ghi
    public function delete($final_id)
    {
        echo "<br>" . __METHOD__;
        echo "<pre>";
        echo 'dữ liệu $final_id lấy từ controller là: '.$final_id;
        echo "</pre>";

        $sqlDelete = "DELETE FROM $this->table1 WHERE `id` = ?";
        $stmtDelete = $this->connection->prepare($sqlDelete);
        $resultDelete = $stmtDelete->execute([$final_id]);
        
        echo "<pre>";
        var_dump($sqlDelete);
        echo "</pre>";

        return $resultDelete;
    }

    //Tìm kiếm
    public function search($dataRaw)
    {
        echo "<br>" . __METHOD__;
        // echo "<pre>Print dataRaw";
        // print_r($dataRaw);
        // echo "</pre>";

        $dataBind = [];
        $dataBind[] = $dataRaw["keyword"];
        $dataBind[] = $dataRaw["orderby"];
        $dataBind[] = $dataRaw["orderdir"];
        $dataBind[] = $dataRaw["continent"];
        $dataBind[] = $dataRaw["startRecord"];
        $dataBind[] = $dataRaw["limitRecord"];

        // echo "<pre>Print dataBind: ";
        // print_r($dataBind);
        // echo "</pre>";

        if (strlen($dataRaw["keyword"])>0)
        {
            $sqlSearch = "  SELECT *,$this->table1.id as c_id FROM $this->table1
                            LEFT JOIN $this->table2 ON $this->table1.c_continent_id = $this->table2.id
                            WHERE ($this->table1.c_name LIKE \"%$dataBind[0]%\"
                            OR $this->table1.c_topleveldomain LIKE \"%$dataBind[0]%\")";

            if (in_array($dataRaw["continent"], ["ctnt_0", "ctnt_1", "ctnt_2", "ctnt_3"]))
            {
                $ctnt_id = str_replace("ctnt_", "", $dataRaw["continent"]);
                $ctnt_id = (int)$ctnt_id;
                $sqlSearch .= "AND $this->table1.c_continent_id = $ctnt_id";
            }      
        }
        else
        {
            $sqlSearch = "  SELECT *,$this->table1.id as c_id FROM $this->table1
                            LEFT JOIN $this->table2 ON $this->table1.c_continent_id = $this->table2.id";

            if (in_array($dataRaw["continent"], ["ctnt_0", "ctnt_1", "ctnt_2", "ctnt_3"]))
            {
                $ctnt_id = str_replace("ctnt_", "", $dataRaw["continent"]);
                $ctnt_id = (int)$ctnt_id;
                $sqlSearch .= " WHERE $this->table1.c_continent_id = $ctnt_id";
            }
        }
        $sqlSearch .= " ORDER BY $dataBind[1] $dataBind[2]";
        $sqlSearch .= " LIMIT $dataBind[4],$dataBind[5]";

        $stmtSearch = $this->connection->prepare($sqlSearch);
        $stmtSearch->execute();
        $resultSearch = $stmtSearch->setFetchMode(PDO::FETCH_OBJ);
        $countries = $stmtSearch->fetchAll();
        return $countries;
    }

    //Đếm số tổng bản ghi
    public function count2($dataRaw)
    {
        echo "<br>" . __METHOD__;
        // echo "<pre>";
        // print_r($dataRaw);
        // echo "</pre>";

        $dataBind = [];
        $dataBind[] = $dataRaw["keyword"];
        $dataBind[] = $dataRaw["continent"];

        if (strlen($dataRaw["keyword"])>0)
        {
            $sqlCount = "   SELECT COUNT(*) AS total FROM $this->table1
                            WHERE ($this->table1.c_name LIKE \"%$dataBind[0]%\"
                            OR $this->table1.c_topleveldomain LIKE \"%$dataBind[0]%\")";

            if (in_array($dataRaw["continent"], ["ctnt_0", "ctnt_1", "ctnt_2", "ctnt_3"]))
            {
                $ctnt_id = str_replace("ctnt_", "", $dataRaw["continent"]);
                $ctnt_id = (int)$ctnt_id;
                $sqlCount .= "AND $this->table1.c_continent_id = $ctnt_id";
            }      
        }
        else
        {
            $sqlCount = "   SELECT COUNT(*) AS total FROM $this->table1";

            if (in_array($dataRaw["continent"], ["ctnt_0", "ctnt_1", "ctnt_2", "ctnt_3"]))
            {
                $ctnt_id = str_replace("ctnt_", "", $dataRaw["continent"]);
                $ctnt_id = (int)$ctnt_id;
                $sqlCount .= " WHERE $this->table1.c_continent_id = $ctnt_id";
            }
        }

        $stmt = $this->connection->prepare($sqlCount);
        $stmt->execute();
        $result = $stmt->setFetchMode(PDO::FETCH_OBJ);
        $count = $stmt->fetchObject();

        // echo "<pre>";
        // print_r($count);
        // echo "</pre>";
        return $count;
    }
}