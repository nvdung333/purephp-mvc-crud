<?php
class CountriesController {

    public $model;
    public function __construct()
    {
        $this->model = new CountriesModel();
    }
    
    //Liệt kê các bản ghi
    public function read() {
        echo "<br>" . __METHOD__;

        $countryModel = new CountriesModel();

        //Đếm tổng số bản ghi khi chưa lọc gì
        $count = $countryModel->count1();
        // echo "<pre>Print Count: ";
        // print_r($count);
        // echo "</pre>";
        
        //Giới hạn số bản ghi trên 1 trang
        if (isset($_REQUEST["limitrecord"]))
        {
            // $errorlimitrecord = [];

            $patternlimitrecord = "/^\d+$/";
            if (preg_match($patternlimitrecord,$_REQUEST["limitrecord"]) != 1)
            {
                $_SESSION["error"] = "Số bản ghi phải là một số!";
            }
            
            // if ($_REQUEST["limitrecord"]==0)
            // {
            //     $errorlimitrecord["b"] = "Số bản ghi phải là một số nguyên dương lớn hơn 0!";
            // }
            // echo"<pre>";
            // print_r($errorlimitrecord);
            // echo"</pre>";
            // $_SESSION["error"] = $errorlimitrecord;

            if ($_REQUEST["limitrecord"] != "") { $limitRecord = (int)$_REQUEST["limitrecord"]; } else { $limitRecord = (int)0; }
        }
        else { $limitRecord = (int)5; }

        //Tổng số button page
        if ($limitRecord!=0)
        {
            $pageTotal = ($count->total)/$limitRecord;
            $pageTotal = ceil($pageTotal);
        }
        else { $pageTotal = 0;}

        //Khai báo mảng chứa thông tin Pagination
        $dataPagination = [];

        //Gán thông tin trang hiện tại vào mảng dataPagination
        if (isset($_REQUEST["page"]))
        { 
            if ($_REQUEST["page"] != "") { $dataPagination["page"] = (int)$_REQUEST["page"]; } else { $dataPagination["page"] = (int)0; }
        }
        else
        {
            $dataPagination["page"] = (int)1;
        }

        //Bản ghi đầu tiên tại trang hiện tại
        $startRecord = ($dataPagination["page"] - 1) * $limitRecord;

        //Gán các thông tin còn lại vào mảng dataPagination
        $dataPagination["limitRecord"] = (int)$limitRecord;
        $dataPagination["pageTotal"] = (int)$pageTotal;
        $dataPagination["startRecord"] = (int)$startRecord;

        // echo "<pre>Print dataPagination: ";
        // print_r($dataPagination);
        // echo "</pre>";

        //Gọi phương thức
        $countries = $countryModel->getAll($dataPagination);

        // echo "<pre>Print Request: ";
        // print_r($_REQUEST);
        // echo "</pre>";


        //Phần Search
        if (isset($_REQUEST["keyword"]) or isset($_REQUEST["orderby"]) or isset($_REQUEST["orderdir"]) or isset($_REQUEST["continent"]))
        {
            $dataRaw = [];
            if ($_REQUEST["keyword"] != "") { $dataRaw["keyword"] = $_REQUEST["keyword"]; } else { $dataRaw["keyword"] = ""; }
            if ($_REQUEST["orderby"] != "") { $dataRaw["orderby"] = $_REQUEST["orderby"]; } else { $dataRaw["orderby"] = "c_id"; }
            if ($_REQUEST["orderdir"] != "") { $dataRaw["orderdir"] = $_REQUEST["orderdir"]; } else { $dataRaw["orderdir"] = "ASC"; }
            if ($_REQUEST["continent"] != "") { $dataRaw["continent"] = $_REQUEST["continent"]; } else { $dataRaw["continent"] = ""; }

            //Đếm tổng số bản ghi sau khi lọc
            $count = $countryModel->count2($dataRaw);
            

            //Phần Pagination
            if ($limitRecord!=0)
            {   $pageTotal = ($count->total)/$limitRecord;
                $pageTotal = ceil($pageTotal);  }
            else { $pageTotal = 0;}

            if (isset($_REQUEST["page"]))
            { 
                if ($_REQUEST["page"] != "") { $curPage = (int)$_REQUEST["page"]; } else { $curPage = (int)0; }
            }
            else { $curPage = (int)1; }
            $startRecord = ($curPage - 1) * $limitRecord;
            
            $dataRaw["limitRecord"] = (int)$limitRecord;
            $dataRaw["startRecord"] = (int)$startRecord;

            // echo "<pre>Print dataRaw: ";
            // print_r($dataRaw);
            // echo "</pre>";

            //Gọi phương thức
            $countries = $countryModel->search($dataRaw);  
        }


        // echo "<pre>";
        // print_r($countries);
        // echo "</pre>";

        // echo "<pre>Print Count: ";
        // print_r($count);
        // echo "</pre>";


        include_once "mvc/views/countries/read.php";
    }

    //Trả về view thêm mới
    public function create() {
        echo "<br>" . __METHOD__;
        include_once "mvc/views/countries/create.php";
    }

    //Lưu data khi thêm mới
    public function store() {
        echo "<br>" . __METHOD__;
        
        $errors_validate = [];

        if (is_array($_POST) & !empty($_POST))
        {
            echo "<pre>Print Post: ";
            print_r($_POST);
            echo "</pre>";

            //Kiểm tra lỗi tên quốc gia
            if (isset($_POST['c_name']) && strlen($_POST['c_name']) > 0)
            { $c_name = $_POST['c_name']; }
            else
            { $errors_validate["c_name"] = "Yêu cầu nhập tên quốc gia / vùng lãnh thổ"; }

            //Kiểm tra lỗi domain quốc gia
            if (isset($_POST['c_topleveldomain']) && strlen($_POST['c_topleveldomain']) > 0)
            { $c_topleveldomain = $_POST['c_topleveldomain']; }
            else
            { $errors_validate["c_topleveldomain"] = "Yêu cầu nhập tên miền quốc gia / vùng lãnh thổ"; }

            //Kiểm tra lỗi ngày gia nhập
            if (isset($_POST['c_joindate']) && strlen($_POST['c_joindate']) > 0)
            { $c_joindate = $_POST['c_joindate']; }
            else
            { $errors_validate["c_joindate"] = "Yêu cầu nhập ngày gia nhập tổ chức"; }

            //Kiểm tra lỗi châu lục
            if (isset($_POST['c_continent_id']))
            {
                if (!in_array($_POST['c_continent_id'], [1,2,3]))
                { $errors_validate["c_continent_id"] = "Yêu cầu chọn lại châu lục"; }
                else
                { $c_continent_id = (int)$_POST['c_continent_id']; }
            }
            else
            { $errors_validate["c_continent_id"] = "Yêu cầu chọn châu lục"; }

            //In ra thông tin file upload
            echo "<pre>Print Files: ";
            print_r($_FILES);
            echo "</pre>";

            //Kiểm tra lỗi upload quốc kỳ
            if (isset($_FILES["c_flag"]["name"]) && isset($_FILES["c_flag"]["tmp_name"]))
            {
                if (strlen($_FILES["c_flag"]["name"]) > 0 && strlen($_FILES["c_flag"]["tmp_name"]) > 0 && strlen($_FILES["c_flag"]["size"]) > 0)
                {
                    $target_dir = "../Project/uploads/";
                    $target_file = $target_dir . basename($_FILES["c_flag"]["name"]);
                    $uploadOK = 1;
    
                    // Kiểm tra xem folder chứa file ảnh tồn tại chưa (nếu chưa thì tạo mới)
                    if (!file_exists($target_dir))
                    {
                        mkdir($target_dir, 0777, true);
                    }
    
                    // Kiểm tra xem có phải file ảnh hay không
                    $check = getimagesize($_FILES["c_flag"]["tmp_name"]);
                    if($check !== false) {
                        echo "<br>File is an image - " . $check["mime"] . ".";
                        $uploadOK = 1;
                    } else {
                        echo "<br>File is not an image.";
                        $uploadOK = 0;
                    }
    
                    // Kiểm tra xem file ảnh tồn tại chưa
                    if (file_exists($target_file)) {
                        echo "<br>Sorry, file already exists.";
                        $uploadOK = 0;
                    }
    
                    // Giới hạn dung lượng tối đa (bytes) cho file ảnh
                    if ($_FILES["c_flag"]["size"] > 3000000) {
                        echo "<br>Sorry, your file is too large.";
                        $uploadOK = 0;
                    }
    
                    // Nếu có lỗi thì không cho phép upload ảnh
                    if ($uploadOK == 0)
                    {
                        $errors_validate["c_flag"] = "Sorry, your file was not uploaded.";
                    }
                    // Nếu không có lỗi thì upload ảnh
                    // else
                    // {
                    //     if (move_uploaded_file($_FILES["c_flag"]["tmp_name"], $target_file))
                    //     {
                    //         $uploadOK = 1;
                    //         echo "<br>Your file has been uploaded.";
                    //     }
                    //     else {
                    //         $errors_validate["c_flag"] = "Sorry, there was an error uploading your file.";
                    //     }
                    // }
                }
                else
                {
                    $errors_validate["c_flag"] = "Chưa tải lên hình ảnh quốc kỳ";
                }
            }
            else
            {
                $errors_validate["c_flag"] = "Chưa tải lên hình ảnh quốc kỳ";
            }


            // In ra lỗi
            echo "<pre>Print Errors: ";
            print_r($errors_validate);
            echo "</pre>";

            $data = [];
            $data[] = isset($_POST["c_name"]) ? $_POST["c_name"] : "";
            $data[] = isset($_POST["c_area"]) ? $_POST["c_area"] : "";
            $data[] = isset($_POST["c_population"]) ? $_POST["c_population"] : "";
            $data[] = isset($target_file) ? $target_file : "";
            $data[] = isset($_POST["c_joindate"]) ? $_POST["c_joindate"] : "";
            $data[] = isset($_POST["c_continent_id"]) ? (int)$_POST["c_continent_id"] : "";
            $data[] = isset($_POST["c_topleveldomain"]) ? $_POST["c_topleveldomain"] : "";
            $data[] = date("Y-m-d H:i:s");
            $data[] = date("Y-m-d H:i:s");

            $dataRaw = [];
            $dataRaw["c_name"] = isset($_POST["c_name"]) ? $_POST["c_name"] : "";
            $dataRaw["c_area"] = isset($_POST["c_area"]) ? $_POST["c_area"] : "";
            $dataRaw["c_population"] = isset($_POST["c_population"]) ? $_POST["c_population"] : "";
            $dataRaw["c_flag"] = isset($target_file) ? $target_file : "";
            $dataRaw["c_joindate"] = isset($_POST["c_joindate"]) ? $_POST["c_joindate"] : "";
            $dataRaw["c_continent_id"] = isset($_POST["c_continent_id"]) ? (int)$_POST["c_continent_id"] : "";
            $dataRaw["c_topleveldomain"] = isset($_POST["c_topleveldomain"]) ? $_POST["c_topleveldomain"] : "";
            $dataRaw["created_at"] = date("Y-m-d H:i:s");
            $dataRaw["updated_at"] = date("Y-m-d H:i:s");
            

            if (empty($errors_validate))
            {
                $checkDuplicateModel = $this->model->checkduplicate1($dataRaw);
                if ($checkDuplicateModel === false)
                {
                    echo "<pre>";
                    echo '<br>Đã nhập trùng thông tin!';
                    echo "</pre>";
                }
                elseif ($checkDuplicateModel === true)
                {
                    //Upload ảnh
                    if (move_uploaded_file($_FILES["c_flag"]["tmp_name"], $target_file))
                    {
                        $uploadOK = 1;
                        echo "<br>Your file has been uploaded.";
                    }
                    else {
                        $errors_validate["c_flag"] = "Sorry, there was an error uploading your file.";
                        // In ra lỗi
                        echo "<pre>Print Errors: ";
                        print_r($errors_validate);
                        echo "</pre>";
                    }

                    // Khởi tạo model
                    $countryModel = new CountriesModel();
                    $countryModel->store($data);

                    // Về trang chủ
                    $_SESSION["flash_message"] = "Thêm mới thành công!!!";
                    $domain =  SITE_URL."index.php?controller=countries&action=read";
                    header("Location: $domain");
                    exit;
                }    
            }
        } 
    }//end store()

    //Trả về view sửa
    public function edit() {
        echo "<br>" . __METHOD__;
        $id = isset($_GET["id"]) ? (int) $_GET["id"] : "";
        $countryModel = new CountriesModel();
        $country = $countryModel->getSingle($id);

        // echo "<pre>";
        // print_r($country);
        // echo "</pre>";

        $_SESSION["update_message"] = "Kiểm tra lại kĩ nội dung trước khi sửa thông tin!";
        include_once "mvc/views/countries/update.php";
    }

    //Sửa data
    public function update() {
        echo "<br>" . __METHOD__;

        echo "<pre>Print Post: ";
        print_r($_POST);
        echo "</pre>";

        $errors_validate = [];

        //Kiểm tra lỗi tên quốc gia
        if (isset($_POST['c_name']) && strlen($_POST['c_name']) > 0)
        { $c_name = $_POST['c_name']; }
        else
        { $errors_validate["c_name"] = "Yêu cầu nhập tên quốc gia / vùng lãnh thổ"; }

        //Kiểm tra lỗi domain quốc gia
        if (isset($_POST['c_topleveldomain']) && strlen($_POST['c_topleveldomain']) > 0)
        { $c_name = $_POST['c_topleveldomain']; }
        else
        { $errors_validate["c_topleveldomain"] = "Yêu cầu nhập tên miền quốc gia / vùng lãnh thổ"; }

        //Kiểm tra lỗi ngày gia nhập
        if (isset($_POST['c_joindate']) && strlen($_POST['c_joindate']) > 0)
        { $c_name = $_POST['c_joindate']; }
        else
        { $errors_validate["c_joindate"] = "Yêu cầu nhập ngày gia nhập tổ chức"; }

        //Kiểm tra lỗi châu lục
        if (isset($_POST['c_continent_id']))
        {
            if (!in_array($_POST['c_continent_id'], [1,2,3]))
            { $errors_validate["c_continent_id"] = "Yêu cầu chọn lại châu lục"; }
            else
            { $c_name = (int)$_POST['c_continent_id']; }
        }
        else
        { $errors_validate["c_continent_id"] = "Yêu cầu chọn châu lục"; }
      
        
        echo "<pre>Print Files: ";
        print_r($_FILES);
        echo "</pre>";

        //Kiểm tra lỗi upload quốc kỳ
        if (isset($_FILES["c_flag"]["name"]) && isset($_FILES["c_flag"]["tmp_name"]))
        {
            if (strlen($_FILES["c_flag"]["name"]) > 0 && strlen($_FILES["c_flag"]["tmp_name"]) > 0 && strlen($_FILES["c_flag"]["size"]) > 0)
            {
                $target_dir = "../Project/uploads/";
                $target_file = $target_dir . basename($_FILES["c_flag"]["name"]);
                $uploadOK = 1;

                // Kiểm tra xem folder chứa file ảnh tồn tại chưa (nếu chưa thì tạo mới)
                if (!file_exists($target_dir))
                {
                    mkdir($target_dir, 0777, true);
                }

                // Kiểm tra xem có phải file ảnh hay không
                $check = getimagesize($_FILES["c_flag"]["tmp_name"]);
                if($check !== false) {
                    echo "<pre>";
                    echo "File is an image - " . $check["mime"] . ".";
                    echo "</pre>";
                    $uploadOK = 1;
                } else {
                    $errors_validate["c_flag_check_is_image"] = "File is not an image.";
                    $uploadOK = 0;
                }

                // Kiểm tra xem file ảnh tồn tại chưa
                if (file_exists($target_file)) {
                    $errors_validate["c_flag_check_exists"] = "Sorry, file already exists.";
                    $uploadOK = 0;
                }

                // Giới hạn dung lượng tối đa (bytes) cho file ảnh
                if ($_FILES["c_flag"]["size"] > 3000000) {
                    $errors_validate["c_flag_check_size"] = "Sorry, your file is too large.";
                    $uploadOK = 0;
                }

                // Nếu có lỗi thì không cho phép upload ảnh
                if ($uploadOK == 0)
                {
                    $errors_validate["c_flag"] = "Sorry, your file was not uploaded.";
                }
            }
        }

        // In ra lỗi
        echo "<pre>Print Errors: ";
        print_r($errors_validate);
        echo "</pre>";

        $dataRaw = [];
        $dataRaw["c_name"] = isset($_POST['c_name']) ? $_POST['c_name'] : "";
        $dataRaw["c_area"] = isset($_POST['c_area']) ? $_POST['c_area'] : "";
        $dataRaw["c_population"] = isset($_POST['c_population']) ? $_POST['c_population'] : "";
        if (isset($target_file)) { $dataRaw["c_flag"] = $target_file; }
        $dataRaw["c_joindate"] = isset($_POST['c_joindate']) ? $_POST['c_joindate'] : "";
        $dataRaw["c_continent_id"] = isset($_POST['c_continent_id']) ? (int)$_POST['c_continent_id'] : "";
        $dataRaw["c_topleveldomain"] = isset($_POST['c_topleveldomain']) ? $_POST['c_topleveldomain'] : "";
        $dataRaw["updated_at"] = date("Y-m-d H:i:s");
        $dataRaw["id"] = isset($_POST['onlyid']) ? (int)$_POST['onlyid'] : "";
        
        if (empty($errors_validate))
        {
            $checkDuplicateModel = $this->model->checkduplicate2($dataRaw);
            if ($checkDuplicateModel === false)
            {
                echo "<pre>";
                echo '<br>Đã nhập trùng thông tin!';
                echo "</pre>";
            }
            elseif ($checkDuplicateModel === true)
            {
                //Upload ảnh
                if (move_uploaded_file($_FILES["c_flag"]["tmp_name"], $target_file))
                {
                    $uploadOK = 1;
                    echo "<br>Your file has been uploaded.";
                }
                else {
                    $errors_validate["c_flag"] = "Sorry, there was an error uploading your file.";
                    // In ra lỗi
                    echo "<pre>Print Errors: ";
                    print_r($errors_validate);
                    echo "</pre>";
                }

                $countryModel = new CountriesModel();
                $countryModel->update($dataRaw);

                // Về trang chủ
                $_SESSION["flash_message"] = "Cập nhật lại thành công!!!";
                $domain =  SITE_URL."index.php?controller=countries&action=read";
                header("Location: $domain");
                exit;
            }
        }
    }//end update()


    //Trả về view xóa
    public function delete() {
        echo "<br>" . __METHOD__;

        $id = isset($_GET["id"]) ? (int) $_GET["id"] : "";
        $countryModel = new CountriesModel();
        $country = $countryModel->getSingle($id);

        // echo "<pre>";
        // print_r($country);
        // echo "</pre>";

        $_SESSION["delete_message"] = "Kiểm tra lại kĩ nội dung trước khi xóa!";
        include_once "mvc/views/countries/delete.php";
    }

    //Xóa data
    public function destroy() {
        echo "<br>" . __METHOD__;

        if (is_array($_POST) & !empty($_POST))
        {
            echo "<pre>";
            print_r($_POST);
            echo "</pre>";

            $id = isset($_POST["onlyid"]) ? $_POST["onlyid"] : "";
            echo "<pre>";
            echo "[onlyid] lấy từ view delete là: ".$id;
            echo "</pre>";

            $countryModel = new CountriesModel();
            $countryModel->delete($id); //truyền id tới model

            // Về trang chủ
            $_SESSION["flash_message"] = "Xóa thành công!!!";
            $domain =  SITE_URL."index.php?controller=countries&action=read";
            header("Location: $domain");
            exit;
        }
    }

    // //Tìm kiếm ($_POST)
    // public function search()
    // {
    //     echo "<br>" . __METHOD__;
        
    //     // echo "<pre>Print Request: ";
    //     // print_r($_REQUEST);
    //     // echo "</pre>";

    //     $dataRaw = [];
    //     if ($_REQUEST["keyword"] != "") { $dataRaw["keyword"] = $_REQUEST["keyword"]; } else { $dataRaw["keyword"] = ""; }
    //     if ($_REQUEST["orderby"] != "") { $dataRaw["orderby"] = $_REQUEST["orderby"]; } else { $dataRaw["orderby"] = "c_id"; }
    //     if ($_REQUEST["orderdir"] != "") { $dataRaw["orderdir"] = $_REQUEST["orderdir"]; } else { $dataRaw["orderdir"] = "ASC"; }
    //     if ($_REQUEST["continent"] != "") { $dataRaw["continent"] = $_REQUEST["continent"]; } else { $dataRaw["continent"] = ""; }

    //     $countryModel = new CountriesModel();
    //     $countries = $countryModel->search($dataRaw);
    //     $count = $countryModel->count2($dataRaw);

    //     // echo "<pre>";
    //     // print_r($count);
    //     // echo "</pre>";

    //     include_once "mvc/views/countries/read.php";
    // }
}