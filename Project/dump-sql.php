<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "countriesmanageproject";

$table1 = "countries";
$table2 = "continents";


// SQL1
try
{
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $sql1= "CREATE TABLE $table1
    (
    id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    c_name VARCHAR(255) DEFAULT'',
    c_area FLOAT(11,2) DEFAULT 0,
    c_population INT(11) DEFAULT 0,
    c_flag VARCHAR(255),
    c_joindate DATE,
    c_continent_id INT(11) UNSIGNED DEFAULT 0,
    c_topleveldomain VARCHAR(255),
    created_at DATETIME,
    updated_at DATETIME
    )";

    $conn->exec($sql1);

    echo "<pre>";
    print_r($sql1);
    echo "<br>Successfully";
    echo "</pre>";
}
catch(PDOException $e)
{
    echo "<pre>";
    echo $sql1 . "<br>" . $e->getMessage();
    echo "</pre>";
}
$conn = null;


// SQL2
try
{
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $sql2 = "CREATE TABLE $table2
    (
    id INT(11) UNSIGNED PRIMARY KEY,
    ctnt_name VARCHAR(255)
    )";

    $conn->exec($sql2);

    echo "<pre>";
    print_r($sql2);
    echo "<br>Successfully";
    echo "</pre>";
}
catch(PDOException $e)
{
    echo "<pre>";
    echo $sql2 . "<br>" . $e->getMessage();
    echo "</pre>";
}
$conn = null;


// SQL3
try
{
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $sql3 = "   INSERT INTO $table2 (`id`, `ctnt_name`)
                VALUES
                (0, 'Chưa xác định'),
                (1, 'Châu lục 1'),
                (2, 'Châu lục 2'),
                (3, 'Châu lục 3')
            ";

    $conn->exec($sql3);

    echo "<pre>";
    print_r($sql3);
    echo "<br>Successfully";
    echo "</pre>";
}
catch(PDOException $e)
{
    echo "<pre>";
    echo $sql3 . "<br>" . $e->getMessage();
    echo "</pre>";
}
$conn = null;


// SQL4
try
{
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $sql4 = "   ALTER TABLE $table1 ADD CONSTRAINT `countries_fk_1`
                FOREIGN KEY (`c_continent_id`) REFERENCES $table2 (`id`) 
                ON DELETE SET NULL
                ON UPDATE CASCADE
            ";

    $conn->exec($sql4);

    echo "<pre>";
    print_r($sql4);
    echo "<br>Successfully";
    echo "</pre>";
}
catch(PDOException $e)
{
    echo "<pre>";
    echo $sql4 . "<br>" . $e->getMessage();
    echo "</pre>";
}
$conn = null;
