<?php
//hằng số tên miền:
define("SITE_URL","http://127.0.0.1:8033/TestProjects/CountriesManageProject/Project/");

//nạp Database
include_once "mvc/Database.php";

//nạp Controllers
include_once "mvc/controllers/CountriesController.php";
include_once "mvc/controllers/Test1Controller.php";
include_once "mvc/controllers/Test2Controller.php";

//nạp Models
include_once "mvc/models/CountriesModel.php";
include_once "mvc/models/Test1Model.php";
include_once "mvc/models/Test2Model.php";

//nạp Route
include_once "mvc/route.php";

//Đặt múi giờ mặc định theo UTC+07:00
date_default_timezone_set('Asia/Bangkok');

//Khởi động session
session_start();

$route = new Router();
$route->run();